#!/bin/bash
#set -e

PATH=$PATH:$HOME/.composer/vendor/bin
export PATH

echo "SetEnv MYSQL_HOST \"$MYSQL_PORT_3306_TCP_ADDR\""  > /etc/httpd/conf.d/mysql_host.conf
echo "EnableSendfile off" >> /etc/httpd/conf.d/mysql_host.conf
sed -i "s/DB_HOST=.*/DB_HOST=$MYSQL_PORT_3306_TCP_ADDR/g" /var/www/html/ec-products/.env

echo '<Directory "/var/www/html/ec-products">' > /etc/httpd/conf.d/ec-crawler.conf
echo 'AllowOverride All' >> /etc/httpd/conf.d/ec-crawler.conf
echo 'Order allow,deny' >> /etc/httpd/conf.d/ec-crawler.conf
echo 'Allow from all' >> /etc/httpd/conf.d/ec-crawler.conf
echo '</Directory>' >> /etc/httpd/conf.d/ec-crawler.conf
exec "$@"
