docker rm -f ec-crawler
docker build -t chunhsin/ec-crawler .
docker run --name "ec-crawler" --privileged=true -h ec-crawler.docker -p 20080:80 --link mysql-dev:mysql -v $(pwd)/../web:/var/www/html:rw -d chunhsin/ec-crawler
docker exec -it ec-crawler /bin/bash