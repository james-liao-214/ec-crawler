docker run -d  \
--name "ec-crawler" \
-h ec-crawler.docker \
-p 20080:80 \
--privileged=true \
--link mysql-dev:mysql \
--link redis-dev:redis \
-v $(pwd)/../web:/var/www/html:rw \
chunhsin/ec-crawler