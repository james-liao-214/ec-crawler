<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('productid' , 100);
            $table->string('title', 500);
            $table->longText('description');
            $table->double('price');
            $table->double('amount');
            $table->string('category1');
            $table->string('category2');
            $table->string('category3');
            $table->string('url' , 1000);
            $table->string('photo' , 1000);
            $table->string('currency');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
