<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Client;
use App\Product;
use App\Jobs\CrawlerJoyceShop;
use App\Jobs\CrawlerPttWeb;
use App\Jobs\CrawlerQueenShop;

use Illuminate\Foundation\Bus\DispatchesJobs;

class EcCrawler extends Command
{
     use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawlers:start {site}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $site = $this->argument('site');
        if(isset($site)){
            if($site == 'joyceshop')
                $this->dispatch(new CrawlerJoyceShop());
            if($site == 'queenshop')
                $this->dispatch(new CrawlerQueenShop());
            if($site == 'ptt')
                $this->dispatch(new CrawlerPttWeb());
        }
        else{
            $this->info('Current Support site : joyceshop,queenshop ');
        //
        }
    }
}
