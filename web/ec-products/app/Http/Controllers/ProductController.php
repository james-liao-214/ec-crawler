<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Product;
use Log;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $type = $request->input('type' , 'json');
        $site = $request->input('site' , 'www.joyce-shop.com');
        $showAll = $request->input('showall' , 0);
        $page = $request->input('p' , 1);
        $offset = $request->input('o' , 10);
        $queryBuilder = Product::orderBy('updated_at', 'desc')
                                ->where('site','like','%'.$site.'%');
        if($showAll == 0){
            $queryBuilder = $queryBuilder->take($offset)
                                         ->skip($offset*($page-1));
        }

        $count = $queryBuilder->count();
        $products = $queryBuilder->get();
        $result = new \stdClass();
        $result->total = $count;
        $result->products = $products;
        if($type== 'xml'){
            $xmlContent = Product::toXML($products);
            return response($xmlContent,200)->header('Content-Type', 'text/xml');
        }
        return response()->json($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        return response('ok', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $content = $request->getContent();
        return response($content, 200);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
