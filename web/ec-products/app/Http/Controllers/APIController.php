<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Jobs\Crawler17Life;
use App\Jobs\MultiCrawlerQueenShop;

class APIController extends Controller
{
    /**
     * Show the specified photo comment.
     *
     * @return Response
     */
    public function getFire17life(){
        $this->dispatch(new Crawler17Life());
        return response("Job Dispatched", 200);
    }

    public function getFirejoyceshop(){
        $this->dispatch(new MultiCrawlerQueenShop());
        return response("Job Dispatched", 200);
    }
}