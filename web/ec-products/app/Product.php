<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;

class Product extends Model
{
	protected $hidden = array(
        'created_at','updated_at','site','p_version' , 'id','udt'
    );
    //
    //
    public static function toXML($products){
    	$hidden = array(
            'created_at','updated_at','site','p_version' , 'id' , 'udt'
        );
        $xmlContent = '<?xml version="1.0" encoding="utf-8"?>'."\n";
        $xmlContent .= '<products version="2.0">'."\n";
        $products = $products->toArray();
        foreach ($products as $product) {
            $xmlContent.="<product>\n";
            foreach ($product as $key => $value) {
                if(!in_array($key, $hidden))
                	$value = htmlspecialchars($value);
                	// if($key == 'url'){

                	// 	$xmlContent.="<$key>".$value."</$key>\n";
                	// }
                	// else
                    $xmlContent.="<$key>$value</$key>\n";
            }
            $xmlContent.="</product>\n";
        }

        $xmlContent .= "</products>";
        return $xmlContent;
    }
}
