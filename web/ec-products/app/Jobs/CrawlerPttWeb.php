<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use Symfony\Component\DomCrawler\Crawler;
use App\Jobs\BasicCrawler;

class CrawlerPttWeb extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->crawler = new BasicCrawler(
            array(
                'baseUrl' => 'https://www.ptt.cc',
                'cacheDB' => '9',
                'baseName' => 'pttbeauty',
                'site' => 'www.ptt.cc',
                'poolConcurrence' => 2,
                'productUrlPattern' => array("/^\/bbs\/Beauty\/M/"),
                'needsGoThroughPattern' => array("/^\/bbs\/Beauty\/index/"),
                'entryPoints' => array('https://www.ptt.cc/bbs/Beauty/index.html'),
                'productMapper' => $this,
                'deep' => 1
            )
        );
        $this->crawler->handle();
    }

    public function parseProduct($res , $url ){
        $body = $res->getBody();
        $crawler = new Crawler();
        $crawler->addContent($body);
        $crawler->filter('span.article-meta-value')
                ->each(function(Crawler $node, $i) {
                    if($i == 0)
                        print "User : ".$node->text()."\n";
                    if($i == 1)
                        print "Board : ".$node->text()."\n";
                    if($i == 2)
                        print "Title : ".$node->text()."\n";
                    if($i == 3)
                        print "Date : ".$node->text()."\n";
                   // print $i;
                });
        $crawler->filter('span.f2')
                ->each(function(Crawler $node, $i) {
                    // print "Content : ".$node->html()."\n";
                    if($i == 0)
                        print "IP : ".$node->text()."\n";
                    if($i == 1)
                        print "Link : ".$node->text()."\n";
                });
        $total_push = 0;
        $crawler->filter('div.push > span.push-tag')
                ->each(function(Crawler $node, $i) use( &$total_push){
                    // if( strpos('f1' , $node->attr('class') ) !=== FALSE){
                    //     $total_push++;
                    // echo $node->text()."\n";
                    if( strpos(trim($node->text()) , '噓' ) !== FALSE ){
                        $total_push --;
                    }
                    if( strpos(trim($node->text()) , '推' ) !== FALSE){
                        $total_push ++;
                    }

                });

        print $total_push ."\n";
        $crawler->filter('#main-content')
                ->each(function(Crawler $node, $i) {
                    // print "Content : ".$node->html()."\n";

                });
        return FALSE;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
}
