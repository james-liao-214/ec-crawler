<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;

use App\Product;
use Redis;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class BasicCrawler {
    var $pLogger;
    var $baseUrl = 'https://www.queenshop.com.tw/';
    private $startTime = 0;
    private $cacheDB = '8';
    private $deep = 50;
    private $baseName = 'queenshop';
    private $site = 'www.queenshop.com.tw';
    var $productUrlPattern = array("/^PDContent.asp/");
    var $needsGoThroughPattern = array("/^PDList[\d].asp/");
    private $debug = false;
    private $getPRoductCount = 0;
    private $poolConcurrence = 10;
    private $waitingLinks = array();
    private $waitingProducts = array();
    private $productMapper = null;
    private $waitSeconds = 0;
    private $entryPoints = array(
        'https://www.queenshop.com.tw/'
    );

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params = array()) {
        foreach ($params as $key => $value) {
            $this->$key = $value;
        }
        $logName = '/var/log/ec-crawler/'.$this->baseName.'.log';
        $stream  = new StreamHandler($logName, Logger::DEBUG);
        $this->pLogger = new Logger('perfromance');
        $this->pLogger->pushHandler($stream);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $this->startWatch();
        $this->clearCache();
        $this->pLogger->addInfo('Start Fetch EntryPoints ');
        $this->currentLevel = 0;
        $this->handleEntryPoint();
        $this->pLogger->addInfo('End Fetch EntryPoints ');
        $this->stopWatch();
    }

    private function startWatch(){
        $this->startTime = time();
    }

    public function clearCache(){
        Redis::command('SELECT', [$this->cacheDB]);
        Redis::command('FLUSHDB', []);
    }

    public function handleEntryPoint(){
        $this->fetchLinks($this->entryPoints);
    }

    public function fetchLinks($urls ){

        $this->waitingLinks = $urls;
        $shopper = $this;

        do{
            $this->currentLevel ++;
            if($this->currentLevel > $this->deep){
                break;
            }
            $requests = array();
            $links = $this->waitingLinks;
            $this->waitingLinks = array();
            $processCount = $this->crawledUrl($links ,
                function($response, $index , $url) use(&$shopper){
                    $links = $shopper->handleResponse($response);
                    $shopper->appendLinks($links);
                }
            );
            if($processCount == 0)
                break;
            $this->pLogger->addDebug('current process request count : '.$processCount);
        }while(!empty($this->waitingLinks));
        //Get Product Urls
        $processCount = $this->crawledUrl($this->waitingProducts ,
            function($response, $index , $url) use(&$shopper){
                $shopper->parseProduct($response,$url);
            }
        );
    }

    public function handleResponse($res){

        $body = $res->getBody();
        $baseUrl = $this->baseUrl;

        $crawler = new Crawler();
        $crawler->addContent($body);
        $productLinks = array();
        $gothroughLinks = array();
        $shopper = $this;
        $links = $crawler->filter('a')->reduce(
            function (Crawler $node, $i) use(&$productLinks , &$gothroughLinks,  &$baseUrl , &$shopper) {
                $target = $node->attr('href');
                $directLink = $baseUrl.$target;
                //判斷要不要繼續往下
                foreach ($shopper->needsGoThroughPattern as $pattern) {
                    preg_match($pattern, $target, $matches);
                    if(!empty($matches)){
                        if(!in_array($directLink,$this->waitingLinks) && !in_array($directLink, $gothroughLinks))
                        $gothroughLinks[] = $directLink;
                        return false;
                    }
                }
                //判斷是否為商品頁
                foreach ($shopper->productUrlPattern as $pattern) {
                    preg_match($pattern , $target, $matches);
                    if(!empty($matches)){
                        if(!in_array($directLink,$this->waitingProducts) && !in_array($directLink, $productLinks))
                            $productLinks[] = $directLink;
                        return false;
                    }
                }
            }
        );
        return array($gothroughLinks, $productLinks);
    }


    public function parseProduct($res , $url ){
        if($this->productMapper){
            if($this->productMapper->parseProduct($res, $url)){
                $this->getPRoductCount++;
            }
        }
        else{
            $this->pLogger->addError('No Product Mapper Exists');
        }
    }

    private function crawledUrl($links , $success_callback , $error_callback = null){
        $shopper = $this;
        $client = new Client();
        // $client->setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36');
        $requests = array();
        foreach ($links as $url) {
            if(self::isUrlCrawled($url))
                continue;
            $this->pLogger->addDebug('Try to fetch link : '.$url);
            $requests[] = new Request('GET', $url);
        }
        if (count($requests) == 0){
            return 0;
        }
        else if(count($requests) == 1 ){
            //處理單個 request
            $response = $client->send($requests[0]);
            $uri = $requests[0]->getUri();
            $url = $uri->__toString();
            $success_callback($response,0,$url);
        }
        else{
            $pool = new Pool($client, $requests, [
                'concurrency' => $this->poolConcurrence,
                'fulfilled' => function ($response, $index) use(&$success_callback,&$requests) {
                    $uri = $requests[$index]->getUri();
                    $url = $uri->__toString();
                    $success_callback($response,$index,$url);
                },
                'rejected' => function ($reason, $index) use(&$shopper,&$requests,&$error_callback) {
                    $uri = $requests[$index]->getUri();
                    $url = $uri->__toString();
                    if( $error_callback ){
                        $error_callback($reason,$index ,$url);
                    }
                    $shopper->pLogger->addError('Fetch Failed '.$url, array($reason));
                }
            ]);
            $promise = $pool->promise();
            $promise->wait();
        }
        return count($requests);
    }

    public function getProduct($productID ){
        try{
            $product = Product::where('productid',$productID)
                ->where('site',$this->site)
                ->firstOrFail();
            $lastUpdate = strtotime($product->updated_at);
            $product->updated_at = date('Y-m-d H:i:s');
            $product->p_version++;
        }
        catch(ModelNotFoundException $e){
            $product = new Product();
            $product->site = $this->site;
            $product->productid = $productID;
        }
        return $product;
    }

    private function appendLinks($links){
        $this->waitingLinks = array_merge($this->waitingLinks,$links[0]);
        $this->waitingProducts = array_merge($this->waitingProducts,$links[1]);
    }

    public function secs_to_str ($duration){
        $periods = array(
            'day' => 86400,
            'hour' => 3600,
            'minute' => 60,
            'second' => 1
        );

        $parts = array();

        foreach ($periods as $name => $dur) {
            $div = floor($duration / $dur);

            if ($div == 0)
                continue;
            else
                if ($div == 1)
                    $parts[] = $div . " " . $name;
                else
                    $parts[] = $div . " " . $name . "s";
            $duration %= $dur;
        }

        $last = array_pop($parts);

        if (empty($parts))
            return $last;
        else
            return join(', ', $parts) . " and " . $last;
    }

    public static function isUrlCrawled($url){
        $key = urlencode($url);
        $count = Redis::get($key);
        if(isset($count)){
            Redis::set( $key , $count +1);
            return TRUE;
        }
        Redis::set( $key , 1);
        Redis::command('EXPIRE', [$key, 3600]);
        return FALSE;
    }

    private function stopWatch(){
        $endTime = time();
        $diffTime = $endTime - $this->startTime;
        $timeStr = $this->secs_to_str($diffTime);
        $this->pLogger->addInfo('Fetch '.$this->getPRoductCount.' Products used '.$timeStr ." (".$diffTime."s)");
    }
}
