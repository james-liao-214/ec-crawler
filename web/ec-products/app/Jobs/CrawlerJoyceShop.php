<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use Symfony\Component\DomCrawler\Crawler;
use App\Product;
use App\Jobs\BasicCrawler;

class CrawlerJoyceShop extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    private $crawler;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(){}

   /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        $this->crawler = new BasicCrawler(
            array(
                'baseUrl' => 'https://www.joyce-shop.com/',
                'cacheDB' => '8',
                'baseName' => 'joyceshop',
                'site' => 'www.joyce-shop.com',
                'productUrlPattern' => array("/^PDContent.asp/"),
                'needsGoThroughPattern' => array("/^PDList.asp/"),
                'entryPoints' => array('www.joyce-shop.com/'),
                'productMapper' => $this
            )
        );
        $this->crawler->handle();
    }

    private function tryToFetchAsJson($body){
        $body_line = explode("\n",$body );
        $productContent = "";
        $inProduct = false;
        foreach ($body_line as $line) {
            if($inProduct){
                if(strpos($line,'valid:') > 0){
                    $inProduct = false;
                }
                $line = trim($line);
                $eee = explode(":",$line , 2);
                if(count($eee) > 1)
                    $line = '"'.$eee[0].'":'.str_replace("'","\"",$eee[1]);

                $productContent .= $line."\n";

                if(! $inProduct ){
                    $productContent .="}\n";
                }
            }
            if(strpos($line,'var product') !== FALSE ){
                $productContent = "{\n";
                $inProduct = true;
            }
        }
        $p = json_decode($productContent);
        return $p;
    }

    private function fetchProductFromJson($p,$url){
        $pId = $p->identifier;
        $product = $this->crawler->getProduct($pId);
        //set category
        $cid = 1;
        foreach ($p->category as $c) {
            if($cid === 4)
                break;
            $key = 'category'.$cid;
            $product->$key = $c;
            $cid ++;
        }
        $product->title = $p->fn;
        $product->description = $p->description;
        // $product->brand = $p->brand;
        $product->currency = $p->currency;
        $product->photo = $p->photo;
        $product->url = $p->url;
        $product->price = $p->price;
        $product->amount = $p->amount;
         // $product->valid = $p->valid;
        $product->save();
        $this->crawler->pLogger->addDebug('Saving Product ['.$pId.'] FROM  '.$url);
        return TRUE;
    }

    private function fetchProductFromHtml($body,$url){
        $crawler = new Crawler();
        $crawler->addContent($body);

        $_productID = '';
        $crawler->filter('.NEW_shop_right_product_top p')
                ->each(function(Crawler $node, $i) use(&$_productID){
                    if($i == 0){
                        $_productID = $node->html();
                    }
                });

        if(empty($_productID))
            return FASLE;
        $product = $this->crawler->getProduct($_productID);
        //check previous product
        $product->url = $url;

        $crawler->filter('.NEW_shop_right_product_top > h3')
                ->each(function(Crawler $node, $i) use(&$product){
                    $product->title = $node->text();
                });

         $crawler->filter('#pdImg')
                ->each(function(Crawler $node, $i) use(&$product){
                    $product->photo = 'https://www.joyce-shop.com'.$node->attr('src');
                });
        $product->save();
        $this->crawler->pLogger->addDebug('Saving Product ['.$pId.'] FROM  '.$url);
        return TRUE;
    }

    public function parseProduct($res , $url ){

        $body = $res->getBody();
        $p = $this->tryToFetchAsJson($body);
        if($p){
            $product = $this->fetchProductFromJson($p,$url);
        }
        else{
            $product = $this->fetchProductFromHtml($body,$url);
        }
        if($product)
            return TRUE;
        return FALSE;
    }

}