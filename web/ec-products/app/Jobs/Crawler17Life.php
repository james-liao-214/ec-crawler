<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Client;

class Crawler17Life extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $baseUrl = 'http://www.17life.com';
        $client = new Client();
        $res = $client->get($baseUrl);
        $body = $res->getBody();

        $crawler = new Crawler();
        $crawler->addContent($body);
        $links = $crawler->filter('a')->reduce(
            function (Crawler $node, $i) {
                $baseUrl = 'http://www.17life.com';
                $pattern = "/^\/199\/[\w-]{36}/";
                $target = $node->attr('href');
                $matches = array();
                preg_match($pattern, $target, $matches);
                if(!empty($matches)){
                    echo $baseUrl.$target."<br />";
                    return true;
                }
                return false;
            }
        );
    }
}
