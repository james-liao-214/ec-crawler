<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;


use Symfony\Component\DomCrawler\Crawler;
use App\Product;
use App\Jobs\BasicCrawler;


class CrawlerQueenShop extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    private $crawler;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(){}

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        $this->crawler = new BasicCrawler(
            array(
                'baseUrl' => 'https://www.queenshop.com.tw/',
                'cacheDB' => '8',
                'baseName' => 'queenshop',
                'site' => 'www.queenshop.com.tw',
                'productUrlPattern' => array("/^PDContent.asp/"),
                'needsGoThroughPattern' => array("/^PDList[\d].asp/"),
                'entryPoints' => array('https://www.queenshop.com.tw/'),
                'productMapper' => $this
            )
        );
        $this->crawler->handle();
    }

    public function parseProduct($res , $url ){
        $body = $res->getBody();
        $crawler = new Crawler();
        $crawler->addContent($body);

        $_productID = '';
        $crawler->filter('input[name="yano"]')
                ->each(function(Crawler $node, $i) use(&$_productID){
                    if($i == 0){
                        $_productID = $node->attr('value');
                    }
                });
        if(empty($_productID))
            return FASLE;
        $product = $this->crawler->getProduct($_productID);
        //Product Title
        $crawler->filter('.pure-g > div.pure-u-md-3-5 > ul.item-text-list-l01 > li')
                ->each(function(Crawler $node, $i) use(&$product){
                    if($i ==0)
                        $product->title = trim($node->text());
                });
        //Product Price
        $crawler->filter('.pure-g > div.pure-u-md-2-5 > ul.item-text-list-r01 > li')
                ->each(function(Crawler $node, $i) use(&$product){
                    if($i ==1){
                        $product->amount = trim($node->text());
                    }
                    else if($i ==2){
                        $product->price = trim($node->text());
                        if($product->amount == '')
                            $product->amount = $product->price;
                    }
                });
        //Product Url
        $product->url = $url;
        $product->currency = 'TWD';
        //Product Image
        $crawler->filter('#pdImg')
                ->each(function(Crawler $node, $i) use(&$product){
                    $product->photo = 'https://www.queenshop.com.tw'.$node->attr('src');
                });
        $this->crawler->pLogger->addDebug('Saving Product ['.$_productID.'] FROM  '.$url);
        $product->save();
        return TRUE;
    }

}